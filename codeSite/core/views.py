from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from codeSite.models import Problems,ProblemSolvedByUser,UserRating
from decimal import *


@login_required
def home(request):
    problem_list = Problems.objects.all()
    return render(request, 'home.html', {"problem_list": problem_list})


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = UserCreationForm()
    return render(request, 'signup.html', {'form': form})


@login_required
def problem_view(request, pid):
    problem = Problems.objects.get(problemID=pid)
    return render(request, 'problem.html', {'problem': problem})


def problem_solution(request, pid):
    problem = Problems.objects.get(problemID=pid)
    user = request.user
    solution_status = request.POST.get('choice', False)
    problemSolved =ProblemSolvedByUser(problemID=problem, userID=user,
                                       status=True if solution_status == "True" else False)
    problemSolved.save()
    userrating = None
    try:
        userrating = UserRating.objects.get(userID=user.id)
    except UserRating.DoesNotExist:
        print 'no records found'

    if userrating is None:
        # setting user initial rating
        if solution_status == "True":
            userrating = UserRating(userID=user, rating=problem.accuracy)
            userrating.save()
        else:
            userrating = UserRating(userID=user, rating=(problem.accuracy*Decimal(0.7)))
            userrating.save()
    else:
        if solution_status == "True":
            problem_rating = problem.accuracy
        else:
            problem_rating = problem.accuracy*Decimal(0.7)
        if userrating.rating > problem.accuracy:
            userrating.rating = float(userrating.rating + problem_rating*Decimal(0.2))
            userrating.save()
        else:
            userrating.rating = problem_rating
            userrating.save()

    try:
        problem_predict = Problems.objects.filter(rating__gte=userrating.rating)
        problem_predict = problem_predict.first()
    except Problems.DoesNotExist:
        problem_predict = None
        print 'ex'

    if solution_status == "True":
        msg = "Congratulations! Your solution is correct and tested"
    else:
        msg = "You missed on some Test case try Hard to improve"

    if problem_predict is None:
        return render(request, 'champion.html', {'message': msg})
    else:
        return render(request, 'predict.html', {'problem': problem_predict,'message': msg})
