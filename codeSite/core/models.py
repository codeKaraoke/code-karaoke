from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User


class ProblemSolvedByUser(models.Model):
    problemID = models.ForeignKey(Problems, on_delete=models.CASCADE)
    userID = models.ForeignKey(User, on_delete=models.CASCADE)
    solution_path = models.CharField(255)
    data_set_solved = models.BinaryField()
    status = models.CharField(255)


class Problems(models.Model):
    problemID = models.BigAutoField(db_column='ID', primary_key=True)
    title = models.CharField(255)
    statement = models.TextField()
    input_para = models.TextField()
    output_para = models.TextField()
    constraint = models.TextField()
    small_input = models.CharField(255)
    small_output = models.CharField(255)
    large_input = models.CharField(255)
    large_output = models.CharField(255)
    accuracy = models.DecimalField(4,2)


class ProblemBytag(models.Model):
    problemID = models.ForeignKey(Problems, on_delete=models.CASCADE)
    tag_name = models.ForeignKey(Tag, on_delete=models.CASCADE)


class Tag(models.Model):
    tag_name = models.CharField(255,db_column='ID', primary_key=True)
    rating = models.DecimalField(5,2)


class UserRating(models.Model):
    userID = models.ForeignKey(User,on_delete=models.CASCADE)
    rating = models.DecimalField(10,5)