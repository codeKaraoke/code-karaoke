# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from models import *
# Register your models here.


admin.site.register(Problems)
admin.site.register(ProblemSolvedByUser)
admin.site.register(UserRating)
admin.site.register(Tag)
admin.site.register(ProblemByTag)