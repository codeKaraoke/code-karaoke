# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Create your models here.
from django.db import models
from django.contrib.auth.models import User


class Problems(models.Model):
    problemID = models.BigAutoField(db_column='ID', primary_key=True)
    title = models.CharField(max_length=255)
    statement = models.TextField(default='No statement')
    input_para = models.TextField(default='No input')
    output_para = models.TextField(default='No output')
    constraints = models.TextField(default='No Constraints')
    small_input = models.CharField(max_length=255, default='No input file')
    small_output = models.CharField(max_length=255, default='No output file')
    large_input = models.CharField(max_length=255, default='No input file')
    large_output = models.CharField(max_length=255, default='No output file')
    accuracy = models.DecimalField(max_digits=4, decimal_places=2, default=0)

    def __str__(self):
        return "Problem Id - %s & Title - %s" % (self.problemID, self.title)


class ProblemSolvedByUser(models.Model):
    problemID = models.ForeignKey(Problems, on_delete=models.CASCADE)
    userID = models.ForeignKey(User, on_delete=models.CASCADE)
    solution_path = models.CharField(max_length=255, default='No solution file')
    data_set_solved = models.CharField(max_length=100, default='Small Data Set')
    status = models.CharField(max_length=255, default='Wrong Answer')

    def __str__(self):
        return "%s & User Id  %s & status %s" % (self.problemID, self.userID, self.status)


class Tag(models.Model):
    tag_name = models.CharField(max_length=255, db_column='ID', primary_key=True)
    rating = models.DecimalField(max_digits=5, decimal_places=2)

    def __str__(self):
        return "Tag - %s" % self.tag_name


class ProblemByTag(models.Model):
    problemID = models.ForeignKey(Problems, on_delete=models.CASCADE)
    tag_name = models.ForeignKey(Tag, on_delete=models.CASCADE)

    def __str__(self):
        return "%s & %s" % (self.problemID, self.tag_name)


class UserRating(models.Model):
    userID = models.ForeignKey(User, on_delete=models.CASCADE)
    rating = models.DecimalField(max_digits=5, decimal_places=2)

    def __str__(self):
        return "User Id - %s & Rating - %s" % (self.userID, self.rating)
